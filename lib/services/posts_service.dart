import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'dart:convert';

import 'package:todo_providers/modals/post_model.dart';

class PostsService with ChangeNotifier {
  List<Post> _items = [];
  List<Post> get items {
    return [..._items];
  }

  Future<void> getPosts() async {
    print('get posts !');
    final url = 'https://jsonplaceholder.typicode.com/posts';
    final url2 = 'http://localhost:3000/students';

    try {
      final response = await http.get(url);
      final extractedData = json.decode(response.body);
      print('\n---\n$extractedData\n\n.');
      if (extractedData == null) {
        return;
      }
      final List<Post> loadedPosts = [];
      extractedData.forEach((post) {
        print('\n\n\npost:\n$post\n\n');
        loadedPosts.add(Post(
          id: post['id'],
          body: post['body'],
          title: post['title'],
          userId: post['userId'],
        ));
      });
      _items = loadedPosts;
      notifyListeners();
    } catch (e) {
      throw (e);
    }
  }
}
