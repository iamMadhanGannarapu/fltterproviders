import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class UserDetails {
  String userId;
  String password;
  String firstName;
  String mobileNo;
  String profileKey;
}

class LoginService with ChangeNotifier {
  UserDetails userDetails = UserDetails();

  setUserDetailsLocally({String userName, String userId}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('userName', userName);
    prefs.setString('userId', userId);
  }

  Future<bool> login(String username, String password) async {
    bool loginSucceeded = true;
    final url = 'https://kalgudi.com/rest/v1/profiles/mobilelogin';
    username = '+919491018558';
    password = '12345678';
    print("service: $username , $password");
    try {
      final response = await http.post(
        url,
        body: json.encode(
            {'userName': username, 'password': password, 'sessionId': ""}),
      );
      final responseData = json.decode(response.body);
      var userData = json.decode(responseData['data']);
      if (responseData['code'] == HttpStatus.ok &&
          response.statusCode == HttpStatus.ok) {
        print('logged-in successfully!');
        userDetails.firstName = userData['firstName'];
        userDetails.mobileNo = userData['mobileNo'];
        userDetails.profileKey = userData['profileKey'];
        userDetails.password = password;
        userDetails.userId = username;
        setUserDetailsLocally(
          userName: userData['firstName'],
        );
        notifyListeners();
      } else {
        loginSucceeded = false;
      }
    } catch (e, stacktrace) {
      print('error message: ${e.message} at: \n$stacktrace');
      loginSucceeded = false;
    }
    return loginSucceeded;
  }
}
