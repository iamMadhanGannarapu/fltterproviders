import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_providers/routes/router_constants.dart';
import 'package:todo_providers/screens/home__screen.dart';
import 'package:todo_providers/screens/login_screen.dart';
import 'package:todo_providers/services/login_service.dart';
import 'package:todo_providers/services/posts_service.dart';
import 'package:todo_providers/services/students_service.dart';
// import './routes/router.dart' as router;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginService>(
            create: (context) => LoginService()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: LoginScreenRoute,
        onGenerateRoute: (RouteSettings routeSettings) {
          switch (routeSettings.name) {
            case LoginScreenRoute:
              return MaterialPageRoute(builder: (context) => LoginScreen());
            case HomeScreenRoute:
              return MaterialPageRoute(builder: (context) {
                PostsService ss = PostsService();
                ss.getPosts();
                return ChangeNotifierProvider<PostsService>(
                  // return ChangeNotifierProvider<StudentService>.value(
                  // value: ss,
                  create: (context) {
                    // print('Creating Student Service');
                    // StudentService ss = StudentService();
                    // ss.getStudents();
                    return ss;
                  },
                  child: HomeScreeen(),
                );
              });
            default:
              return null;
          }
        },
      ),
    );
  }
}
