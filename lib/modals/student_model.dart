import 'package:flutter/foundation.dart';

class Student {
  final int id;
  final String firstName;
  final String lastName;
  final String email;
  final String gender;

  Student({
    @required this.id,
    @required this.email,
    @required this.firstName,
    @required this.lastName,
    @required this.gender,
  });

  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
      id: json['id'] as int,
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      email: json['email'] as String,
      gender: json['gender'] as String,
    );
  }
}