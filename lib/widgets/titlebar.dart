import 'package:flutter/material.dart';

class TitleBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  _TitleBarState createState() => _TitleBarState();

  @override
  Size get preferredSize => AppBar().preferredSize;
}

class _TitleBarState extends State<TitleBar> {
  @override
  Widget build(BuildContext context) {
    
    return AppBar(
      title: Text('title bar'),
    );
  }
}
