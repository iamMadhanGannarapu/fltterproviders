import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_providers/modals/student_model.dart';
import 'package:todo_providers/services/login_service.dart';
import 'package:todo_providers/services/posts_service.dart';
import 'package:todo_providers/services/students_service.dart';

class HomeScreeen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.grey[100],
        body: Column(
          children: <Widget>[
            Consumer<LoginService>(
              builder: (_, model, __) {
                return Container(
                  margin: EdgeInsets.all(5),
                  height: 50,
                  width: double.infinity,
                  child: Card(
                    color: Colors.black,
                    margin: EdgeInsets.all(0),
                    child: Text(
                      '${model.userDetails.firstName}',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                );
              },
            ),
            Container(
              height: 650,
              child: Consumer<PostsService>(
                builder: (_, _postsModel, __) {
                  var posts = _postsModel.items;
                  return Padding(
                    padding: EdgeInsets.all(10),
                    child: ListView.builder(
                      itemCount: posts.length,
                      itemBuilder: (_, i) => GestureDetector(
                        onTap: () {
                          print(posts[i].id);
                        },
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(posts[i].id.toString()),
                            Text(posts[i].userId.toString()),
                            Text(posts[i].title),
                            Text(posts[i].body),
                            Divider()
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
