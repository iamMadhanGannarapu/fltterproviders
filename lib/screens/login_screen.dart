import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_providers/routes/router_constants.dart';
import 'package:todo_providers/services/login_service.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  EdgeInsetsGeometry margin = EdgeInsets.only(left: 30, right: 30);
  _login() async {
    String username = _usernameController.text;
    String password = _passwordController.text;
    bool loginSucceeded =
        await Provider.of<LoginService>(context, listen: false)
            .login(username, password);
    if (loginSucceeded) {
      Navigator.pushNamed(context, HomeScreenRoute);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 200,
            ),
            // Container(
            //   margin: margin,
            //   child: TextFormField(
            //     decoration: InputDecoration(
            //         prefixIcon: Icon(
            //           Icons.person,
            //           color: Colors.black26,
            //         ),
            //         labelText: 'User Id',
            //         hintText: 'Mobile / Email'),
            //     controller: _usernameController,
            //   ),
            // ),
            // Container(
            //   margin: margin,
            //   child: TextFormField(
            //     decoration: InputDecoration(
            //         prefixIcon: Icon(
            //           Icons.lock,
            //           color: Colors.black26,
            //         ),
            //         labelText: 'Password',
            //         hintText: 'Password'),
            //     controller: _passwordController,
            //     obscureText: true,
            //   ),
            // ),
            Center(
              child: RaisedButton(
                color: Colors.blue,
                onPressed: _login,
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
